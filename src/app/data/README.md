ABOUT



TECH STACK/DEPENDENCIES	
	-ROS
	-python3
	-OpenCV
        	Follow installation instruction here https://software.intel.com/en-us/openvino-toolkit/choose-download/free-download-linux to download latest version.
	-Raspbian Stretch Lite
	-OpenVINO

	
ENVIRONMENT/DEPENDENCY SETUP
	RPi setup



RUNNING APP(s)
	SETUP
		MODEL CONVERSION
			It is necessary to convert the model files to their IR form before they can be used by the movidius NCS2.The models have already been trained
			on the object, face, etc that is desired. The following steps will describe how to do this conversion.
			
			STEPS:			
			1) Move to the directory with the model_optimizer toolkit
				cd /opt/intel/computer*/deployment_tools/model_optimizer
			2) Determine which model optimizer you will use. The names are similar to those of the deep learning tools with the format being mo_<tool>.py.
			Unless your model has been highly customized the general mo.py will suffice

			The exact arguments needed for the specific optimizer vary and can be looked up on the intel website
			( https://docs.openvinotoolkit.org/latest/_docs_MO_DG_prepare_model_convert_model_Converting_Model.html )
			The NCS2 only supports models in FP16 so --data_type FP16 should be added to the command line command when running the optimizer and
			output path should be specified as the output path defaults to the model optimizer directory which can only be written to if sudo is used 
			
			3a) For caffe models used at various points in this project the following command was sufficient:
				python3 mo.py --input_model <path/to/.caffemodel/file> --input_proto <path/to/.prototxt/file> --data_type FP16 --output_dir <path/to/output/dir>

				ISSUE!!!: When the model is converted using the optimizer the model no longer seems to work as well. For MobileNEt_SSD example the original .caffemodel 
                                and .prototxt files were loaded and the camera focused on a chair producing detections with above 80% confidence, but after conversion using the optimizer
                                the chair was no longer detected.

			    
		INITIALIZING ROS HEIRARCHY (http://wiki.ros.org/roscore)
			In order to run the applications with comunication capabilities using ROS between the edge devices and the central server the ROS master must be running.
			When the master device has been chosen do the following to start the master.
			1) Check to insure that ROS is installed on the device
			2) From the command line terminal on the master device type:  roscore 
			3) Any slave devices on the same network must be made aware of the master device location shown when using roscore in the print out:
							auto-starting new master
							process[master]: started with pid [12651]
							ROS_MASTER_URI=http://ARI-Precision-T1600:11311/
			The ROS_MASTER_URI specifies the address of the master node which must be given to each slave device with the hostname swapped for the hostIP which can be 
			found using "hostname -I" in the terminal of the master device. 
			4) On each of the slave devices run "export ROS_MASTER_URI=http://YourMasterIP:MasterPort/" or append the command to the slave device .bashrc and source it.

			Without step 4 the slave devices can not communicate with the master device so no messages or data can be passed between any of the devices in the system.


FUTURE IDEAS/PROJECT EXTENSIONS
	Visualization Platform
        Object reidentification on signle camera
	Object recognition on untrained cameras





