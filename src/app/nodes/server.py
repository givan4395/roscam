#!/usr/bin/env python
import rospy
import json
from std_msgs.msg import String
from app.msg import found, serv_resp

CAMERA_LIST = {}
ADJACENT = {}
PREVIOUS_FOUND = None

def callback(data):
    global CAMERA_LIST
    global PREVIOUS_FOUND
    global ADJACENT

    #add camera to list of known cameras
    if data.IPAddr not in CAMERA_LIST:
        print("Adding camera")
        ADJACENT[data.IPAddr] = {}

        pub = rospy.Publisher(data.node_name, serv_resp, queue_size=10)
        CAMERA_LIST[data.IPAddr] = pub
        
        #for cam in CAMERA_LIST:
        #    print("LIST: ", cam)

    if data.purpose == "Found" and data.IPAddr != PREVIOUS_FOUND:
        print("Target "+ data.mess + " found by IPcam: "+ data.IPAddr)
        PREVIOUS_FOUND = data.IPAddr



def serv_msg():
    msg = serv_resp()
    msg.purpose = "Update"
    msg.mess = "TESTING"

    return msg



def listener():

    rospy.init_node('Server', anonymous=True)

    #SUB to NEW_CAM to listen for new cameras as they are added and pub list to CAM_REG
    #rospy.Subscriber("NEW_CAM", String, NEW_CAM_callback, pub)
    rospy.Subscriber("FOUND", found, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    print("Starting server")
    listener()
