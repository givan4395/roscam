#!/usr/bin/python3

from __future__ import print_function
import sys
import os
import cv2
import time
import json
import rospy
import rospkg
import datetime
import logging as log
from std_msgs.msg import String
from argparse import ArgumentParser
from openvino.inference_engine import IENetwork, IEPlugin
from subprocess import  check_output
from app.msg import found, serv_resp

# Needed to use picamera as image source on RPis
# Comment out the two import lines below when using on a non-RPi system
import picamera
import picamera.array


ARGS         =   None
MODEL_XML    =   None
MODEL_BIN    =   None
TARGET_LABEL =   None
CAMERA       =   None
IPAddr       =   None
NODE_NAME    =   None



def build_argparser():
    parser = ArgumentParser(description="Intel Movidius Neural Compute Stick camera application" )
    parser.add_argument("-m", "--model", help="Path to an .xml file with a trained model.", default='/home/pi/rosbots_catkin_ws/src/app/data/model/frozen_inference_graph.xml', type=str)
    parser.add_argument("-l", "--cpu_extension", help="MKLDNN (CPU)-targeted custom layers.Absolute path to a shared library with the kernels impl.", type=str, default=None)
    parser.add_argument("-d", "--device", help="Specify the target device-CPU, GPU, FPGA or MYRIAD is acceptable", default="MYRIAD", type=str)
    parser.add_argument("--labels", help="Labels mapping file", default="/home/pi/rosbots_catkin_ws/src/app/data/model/labels.txt", type=str)
    parser.add_argument("-pt", "--prob_threshold", help="Probability threshold for detections filtering", default=0.65, type=float)

    # make default cam on non-RPi devices
    parser.add_argument("-i", "--input", help="Path to video file/images or cam or Picam", default="picam", type=str)

    return parser

# Plugin initialization for specified device and load extensions library if specified
def open_NCS_device():
    log.info("Initializing plugin for {} device...".format(ARGS.device))
    plugin = IEPlugin(device=ARGS.device)
    if ARGS.cpu_extension and 'CPU' in ARGS.device:
        plugin.add_cpu_extension(ARGS.cpu_extension)

    return plugin


# Load NN model for use on device 
def load_model(plugin):
    # Read in Graph file (IR)
    log.info("Reading IR...")
    net = IENetwork(model=MODEL_XML, weights=MODEL_BIN)

    assert len(net.inputs.keys()) == 1, "Supports only single input topologies"
    assert len(net.outputs) == 1, "Supports only single output topologies"
    input_blob = next(iter(net.inputs))
    out_blob = next(iter(net.outputs))

    # Load network to the plugin
    log.info("Loading IR to the plugin...")
    exec_net = plugin.load(network=net, num_requests=2)

    return input_blob, out_blob, exec_net, net


#Set up nodes and topics for camera
def set_ROS_cam():

    #Create a camera node using IPaddress
    rospy.init_node("CAM", anonymous=True)
    global NODE_NAME
    NODE_NAME = rospy.get_name()

    # Listen for camera list from server
    rospy.Subscriber(NODE_NAME, serv_resp, NC_callback)

    # Make PUB for camera
    pub = rospy.Publisher("FOUND", found, queue_size=10)

    # Give some time  for pub and sub setup
    #time.sleep(5)

    return pub


def NC_callback(data):
    print("Master tried to send list")
    print(data.mess)


# Load labels file 
def get_labels():
    if ARGS.labels:
        with open(ARGS.labels, 'r') as f:
            labels_map = [x.strip() for x in f]
    else:
        labels_map = None

    return labels_map



def main():
    log.basicConfig(format="[ %(levelname)s ] %(message)s", level=log.INFO, stream=sys.stdout)

    #STEP 1: Start device
    plugin = open_NCS_device()

    #STEP 2: Load Neural Network
    input_blob, out_blob, exec_net, net = load_model(plugin)


    # Read and pre-process input image
    n, c, h, w = net.inputs[input_blob].shape
    del net

    pub = set_ROS_cam()

    log.info("Starting inference in sync mode...")
    log.info("To switch between sync and async modes press Tab button")
    log.info("To stop the execution press Esc button")
    cur_request_id = 0
    next_request_id = 1
    is_async_mode = False
    render_time = 0
    ret, frame = CAMERA.read()

    while CAMERA.isOpened():
        if is_async_mode:
            ret, next_frame = CAMERA.read()
        else:
            ret, frame = CAMERA.read()
        if not ret:
            break
        initial_w = CAMERA.get(3)
        initial_h = CAMERA.get(4)

        # Main sync point:
        # in the truly Async mode we start the NEXT infer request, while waiting for the CURRENT to complete
        # in the regular mode we start the CURRENT request and immediately wait for it's completion
        inf_start = time.time()
        if is_async_mode:
            in_frame = cv2.resize(next_frame, (w,h))
            in_frame = in_frame.transpose((2, 0, 1))  # Change data layout from HWC to CHW
            in_frame = in_frame.reshape((n, c, w, h))
            exec_net.start_async(request_id=next_request_id, inputs={input_blob: in_frame})
        else:
            in_frame = cv2.resize(frame, (w, h))
            in_frame = in_frame.transpose((2, 0, 1))  # Change data layout from HWC to CHW
            in_frame = in_frame.reshape((n, c, h, w))
            exec_net.start_async(request_id=cur_request_id, inputs={input_blob: in_frame})
        if exec_net.requests[cur_request_id].wait(-1) == 0:
            inf_end = time.time()
            det_time = inf_end - inf_start

            # Parse detection results of the current request
            result = exec_net.requests[cur_request_id].outputs[out_blob]

            #Draw boxes
            visualize_output(result, frame, initial_w, initial_h, pub, is_async_mode, render_time, cur_request_id, det_time)

        render_start = time.time()
        render_end = time.time()
        render_time = render_end - render_start

        if is_async_mode:
            cur_request_id, next_request_id = next_request_id, cur_request_id
            frame = next_frame

        key = cv2.waitKey(1)
        if key == 27:
            break
        if (9 == key): #tab
            is_async_mode = not is_async_mode
            log.info("Switched to {} mode".format("async" if is_async_mode else "sync"))

    cv2.destroyAllWindows()
    del exec_net
    del plugin


def main_pi():
    log.basicConfig(format="[ %(levelname)s ] %(message)s", level=log.INFO, stream=sys.stdout)

    #STEP 1: Start device
    plugin = open_NCS_device()

    #STEP 2: Load Neural Network
    input_blob, out_blob, exec_net, net = load_model(plugin)


    # Read and pre-process input image
    n, c, h, w = net.inputs[input_blob].shape
    del net

    pub = set_ROS_cam()

    log.info("Starting inference in sync mode...")
    log.info("To switch between sync and async modes press Tab button")
    log.info("To stop the execution press Esc button")
    cur_request_id = 0
    next_request_id = 1
    is_async_mode = False
    render_time = 0

    while True:
        if is_async_mode:
            next_frame = picamera.array.PiRGBArray( CAMERA )
            CAMERA.capture( next_frame, "bgr", use_video_port=True )
        else:
            frame = picamera.array.PiRGBArray( CAMERA )
            CAMERA.capture( frame, "bgr", use_video_port=True )

        initial_w = 620
        initial_h = 480

        # Main sync point:
        # in the truly Async mode we start the NEXT infer request, while waiting for the CURRENT to complete
        # in the regular mode we start the CURRENT request and immediately wait for it's completion
        inf_start = time.time()
        if is_async_mode:
            in_frame = cv2.resize(next_frame.array, (w,h))
            in_frame = in_frame.transpose((2, 0, 1))  # Change data layout from HWC to CHW
            in_frame = in_frame.reshape((n, c, w, h))
            exec_net.start_async(request_id=next_request_id, inputs={input_blob: in_frame})
        else:
            in_frame = cv2.resize(frame.array, (w, h))
            in_frame = in_frame.transpose((2, 0, 1))  # Change data layout from HWC to CHW
            in_frame = in_frame.reshape((n, c, h, w))
            exec_net.start_async(request_id=cur_request_id, inputs={input_blob: in_frame})
            
        if exec_net.requests[cur_request_id].wait(-1) == 0:
            inf_end = time.time()
            det_time = inf_end - inf_start

            # Parse detection results of the current request
            result = exec_net.requests[cur_request_id].outputs[out_blob]

            #Draw boxes
            visualize_output(result, frame.array, initial_w, initial_h, pub, is_async_mode, render_time, cur_request_id, det_time)

        render_start = time.time()
        render_end = time.time()
        render_time = render_end - render_start
        
        frame.seek( 0 )
        frame.truncate()

        if is_async_mode:
            cur_request_id, next_request_id = next_request_id, cur_request_id
            frame = next_frame

        key = cv2.waitKey(1)
        if key == 27:
            break
        if (9 == key): #tab
            is_async_mode = not is_async_mode
            log.info("Switched to {} mode".format("async" if is_async_mode else "sync"))

    cv2.destroyAllWindows()
    del exec_net
    del plugin
    CAMERA.close()



def visualize_output(result, frame, initial_w, initial_h, pub, is_async_mode, render_time, cur_request_id, det_time):
    for obj in result[0][0]:
        # Draw only objects when probability more than specified threshold
        if obj[2] > ARGS.prob_threshold:
            # Extract top-left & bottom-right coordinates of detected objects and object class
            xmin = int(obj[3] * initial_w * 0.9)
            ymin = int(obj[4] * initial_h * 0.9)
            xmax = int(obj[5] * initial_w * 0.9)
            ymax = int(obj[6] * initial_h * 0.9)
            class_id = int(obj[1])

            # Draw box and label\class_id
            color = (min(class_id * 12.5, 255), min(class_id * 7, 255), min(class_id * 5, 255))
            det_label = LABELS[class_id] if LABELS else str(class_id)
            cv2.rectangle(frame, (xmin, ymin), (xmax, ymax), color, 2)
            cv2.putText(frame, det_label + ' ' + str(round(obj[2] * 100, 1)) + ' %', (xmin, ymin - 7), cv2.FONT_HERSHEY_COMPLEX, 0.6, color, 1)

            if TARGET_LABEL in det_label:
                now = datetime.datetime.now()
                current_time = now.strftime("%H:%M:%S")
                print("Target Found {} at {}".format(det_label, current_time))
                msg = msg_found("Found", IPAddr, "Found {}".format(det_label))
                pub.publish(msg)

            inf_time_message = "Inference time: N\A for async mode" if is_async_mode else "Inference time: {:.3f} ms".format(det_time * 1000)
            render_time_message = "OpenCV rendering time: {:.3f} ms".format(render_time * 1000)
            async_mode_message = "Async mode is on. Processing request {}".format(cur_request_id) if is_async_mode else \
                "Async mode is off. Processing request {}".format(cur_request_id)

            cv2.putText(frame, inf_time_message, (15, 15), cv2.FONT_HERSHEY_COMPLEX, 0.5, (200, 10, 10), 1)
            cv2.putText(frame, render_time_message, (15, 30), cv2.FONT_HERSHEY_COMPLEX, 0.5, (10, 10, 200), 1)
            cv2.putText(frame, async_mode_message, (10, int(initial_h - 20)), cv2.FONT_HERSHEY_COMPLEX, 0.5, (10, 10, 200), 1)
    cv2.imshow("Detection Results", frame)



def msg_found(purp, ip, data):
    msg = found()
    msg.purpose = purp
    msg.IPAddr = ip
    msg.mess = data
    msg.node_name = NODE_NAME

    return msg


def get_IP():
    IPAddr = check_output(['hostname','-I']).decode('utf-8').replace(' \n','')

    return IPAddr


if __name__ == '__main__':
    TARGET_LABEL = "stapler"
    ARGS = build_argparser().parse_args()
    IPAddr = get_IP()
    #.model and .bin must have same prefix
    MODEL_XML = ARGS.model
    MODEL_BIN = os.path.splitext(MODEL_XML)[0] + ".bin"
    # Get labels
    LABELS = get_labels()
    
    if ARGS.input == "cam":
        CAMERA = cv2.VideoCapture(0, cv2.CAP_V4L)
        CAMERA.set(cv2.CAP_PROP_FRAME_WIDTH, 620)
        CAMERA.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        sys.exit(main() or 0)

    else:
        CAMERA = picamera.PiCamera()
        CAMERA.resolution = ( 620, 480 )
        CAMERA.vflip = True
        sys.exit(main_pi() or 0)


