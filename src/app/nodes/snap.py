#!/usr/bin/python3

import cv2
import numpy 
from time import gmtime, strftime

cap = cv2.VideoCapture(0, cv2.CAP_V4L)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 620)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

count = 0

while(True):
    ret, frame = cap.read()
    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2BGRA)
    cv2.imshow('frame', rgb)
    count=(count +1)%50
    if not count:
        print("Capturing")
        out = strftime("%H:%M:%S", gmtime())+".jpg"
        cv2.imwrite("captures/"+out, frame)

    if (cv2.waitKey(1) & 0xFF) == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
