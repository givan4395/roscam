#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String 
import numpy as np

import mvnc.mvncapi as mvnc

def talker():
    testing = np.array([1,2,3])
    pub = rospy.Publisher('chatter', String, queue_size=10) #Will publish to chatter topic using strings
    rospy.init_node('talker', anonymous=True) #Create a node named talker 
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()

if __name__ == '__main__':
    try:
        print("Hello")
        talker()
    except rospy.ROSInterruptException:
        pass
