#!/usr/bin/python3

# USAGE
# python object_tracker.py --prototxt deploy.prototxt --model res10_300x300_ssd_iter_140000.caffemodel

# import the necessary packages
from pyimagesearch.centroidtracker import CentroidTracker
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import cv2
import pickle
import os


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/model/deploy.prototxt", help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/model/res10_300x300_ssd_iter_140000.caffemodel", help="path to Caffe pre-trained model")
ap.add_argument("-c", "--confidence", type=float, default=0.5, help="minimum probability to filter weak detections")
ap.add_argument("-s", "--cam_source", type=int, default=0, help="Number of device used for streaming (-1 for picam, >0 for USB cams)")

ap.add_argument("-e", "--embedding-model", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/openface.nn4.small2.v1.t7",help="path to OpenCV's deep learning face embedding model")
ap.add_argument("-r", "--recognizer", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/output/recognizer.pickle",help="path to model trained to recognize faces")
ap.add_argument("-l", "--le", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/output/le.pickle",help="path to label encoder")

ap.add_argument("-x", "--xml", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/model/res10_300x300_ssd_iter_140000.xml", help="path to optimized model xml file" )
ap.add_argument("-b", "--bin", default="/home/jmwolff3/catkin_ws/src/simple_track/nodes/model/res10_300x300_ssd_iter_140000.bin", help="path to optimized model bin file" )
args = vars(ap.parse_args())



# initialize our centroid tracker and frame dimensions
ct = CentroidTracker()
(H, W) = (None, None)

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
#net = cv2.dnn.Net_readFromModelOptimizer(args["xml"], args["bin"])

# specify the target device as the Myriad processor on the NCS
#net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)            ## loads DNN to myriad in

# load our serialized face embedding model from disk
print("[INFO] loading face recognizer...")
#embedder = cv2.dnn.readNetFromTorch(args["embedding_model"])

# load the actual face recognition model along with the label encoder
#recognizer = pickle.loads(open(args["recognizer"], "rb").read())
#le = pickle.loads(open(args["le"], "rb").read())

# initialize the video stream and allow the camera sensor to warmup
print("[INFO] starting video stream...")
if(args["cam_source"] == -1):
    vs = VideoStream(usePiCamera=True).start()
else:
    vs = VideoStream(src=args["cam_source"]).start()
time.sleep(2.0)

fps = FPS().start()

# loop over the frames from the video stream
while True:
	# read the next frame from the video stream and resize it
	frame = vs.read()
	frame = imutils.resize(frame, width=400)

	# if the frame dimensions are None, grab them
	if W is None or H is None:
		(H, W) = frame.shape[:2]

	# construct a blob from the frame, pass it through the network,
	# obtain our output predictions, and initialize the list of
	# bounding box rectangles
	blob = cv2.dnn.blobFromImage(frame, 1.0, (W, H), (104.0, 177.0, 123.0))
       
        # pass the blob through the network and obtain the detections and
        # predictions
	net.setInput(blob)
	detections = net.forward()

	rects = []

	#STEP 1) Identify faces and determine boxes position

	# loop over the detections
	for i in range(0, detections.shape[2]):
		# filter out weak detections by ensuring the predicted
		# probability is greater than a minimum threshold
		if detections[0, 0, i, 2] > args["confidence"]:
			# compute the (x, y)-coordinates of the bounding box for
			# the object, then update the bounding box rectangles list
			box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
			rects.append(box.astype("int"))

			# draw a bounding box surrounding the object so we can visualize it
			(startX, startY, endX, endY) = box.astype("int")
			cv2.rectangle(frame, (startX, startY), (endX, endY),(0, 255, 0), 2)

        # STEP2: Identify if faces seen before and update records
	# update our centroid tracker using the computed set of bounding box rectangles
	objects = ct.update(rects)

        # STEP3: Label face for display
	# loop over the tracked objects
	for (objectID, centroid) in objects.items():
		# draw both the ID of the object and the centroid of the object on the output frame
		text = "ID {}".format(objectID)
		cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
		cv2.circle(frame, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

	# show the output frame
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	fps.update()

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break


# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
vs.stop()
cv2.destroyAllWindows()







