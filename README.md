﻿ABOUT



TECH STACK/DEPENDENCIES	
-ROS (Kinetic)
      DEV machine
	Follow installation instructions here
            http://wiki.ros.org/kinetic/Installation/Ubuntu
      Rpi
      Follow installation instructions here
      http://wiki.ros.org/ROSberryPi/Installing%20ROS%20Kinetic%20on%20the%20Raspberry%20Pi

-python3
      1) open a terminal
      2) enter command sudo apt-get install python3.5
      3) enter sudo password if prompted
      4) check installation using python3 –version which should output python3.5.x

-OpenCV
  	Follow installation instruction here https://software.intel.com/en-us/openvino-toolkit/choose-download/free-download-linux to download latest version.

-Raspbian Stretch Lite
	1) Download latest version 
-OpenVINO

	
ENVIRONMENT/DEPENDENCY SETUP
RPi setup




RUNNING APP(s)
SETUP
	MODEL CONVERSION
	It is necessary to convert the model files to their IR form before they can be used by the movidius NCS2.The models have already been trained on the object, face, etc that is desired. The following steps will describe how to do this conversion.
			
	STEPS:			
		1) Move to the directory with the model_optimizer toolkit
			cd /opt/intel/computer*/deployment_tools/model_optimizer
		2) Determine which model optimizer you will use. The names are similar to those of the deep learning tools with the format being mo_<tool>.py.
Unless your model has been highly customized the general mo.py will suffice

		The exact arguments needed for the specific optimizer vary and can be looked up on the intel website
			( https://docs.openvinotoolkit.org/latest/_docs_MO_DG_prepare_model_convert_model_Converting_Model.html )
		The NCS2 only supports models in FP16 so --data_type FP16 should be added to the command line command when running the optimizer and
		output path should be specified as the output path defaults to the model optimizer directory which can only be written to if sudo is used 
			
		3a) For caffe models used at various points in this project the following command was sufficient:
			python3 mo.py --input_model <path/to/.caffemodel/file> --input_proto <path/to/.prototxt/file> --data_type FP16 --output_dir <path/to/output/dir>

ISSUE!!!: When the model is converted using the optimizer the model no longer seems to work as well. For MobileNEt_SSD example the original .caffemodel and .prototxt files were loaded and the camera focused on a chair producing detections with above 80% confidence, but after conversion using the optimizer the chair was no longer detected.

			    
	INITIALIZING ROS HEIRARCHY (http://wiki.ros.org/roscore)
		In order to run the applications with comunication capabilities using ROS between the edge devices and the central server the ROS master must be running.
When the master device has been chosen do the following to start the master.
		1) Check to insure that ROS is installed on the device
		2) From the command line terminal on the master device type:  roscore 
		3) Any slave devices on the same network must be made aware of the master device location shown when using roscore in the print out:
				auto-starting new master
				process[master]: started with pid [12651]
				ROS_MASTER_URI=http://ARI-Precision-T1600:11311/
		The ROS_MASTER_URI specifies the address of the master node which must be given to each slave device with the hostname swapped for the hostIP which can be 
found using "hostname -I" in the terminal of the master device. 
		4) On each of the slave devices run "export ROS_MASTER_URI=http://YourMasterIP:MasterPort/" or append the command to the slave device .bashrc and source it.

		Without step 4 the slave devices can not communicate with the master device so no messages or data can be passed between any of the devices in the system.






















TESTING
Each application was run using python3 <app_name> from their respective directories in ~/catkin_ws/src/nodes on the dev computer and in ~/rosbots_catkin_ws/src/nodes. The scenes in the live video streams had the same objects/targets and were run for approximately 160 seconds on the dev computer and the RPi. During testing of the object detector two chairs were placed in front of the camera to be detected. For the face tracker a magazine with a woman’s face was placed in front of the camera and moved into and out of frame in the same way during each application run. The Face Recognizer was pre-trained to recognize my face, as well as the face of my colleague Jagadeesh Yedetore, and then tested by moving my face into and out of frame in the same way for each application run.

CPU testing phase -- The application was run using the Openvino inference plugin without modification for inferencing on the NCS2.

NCS2 testing phase -- The application was run using the OpenVino inference plugin with the NCS2 set as the inference target device using
<inference_object name>.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)

NCS2 Optimizer testing phase -- The model was converted to it’s IR form using the OpenVino model optimizer and set to run on the NCS2 in a similar way as in the NCS2 testinng phase


-Object Detector (obj/obj_cpu.py  &  obj/obj_vino.py)
The application was run using MobileNetSSD_deploy.caffemodel and MobileNetSSD_deploy.prototxt

- Face Tracker (/simple_tracker/object_tracker.py)

- Face Recognizer (/rscam/recognize_video.py)

APP
CPU
NCS2
NCS2 Optimizer

Object detector
29.25 FPS
34.26 FPS
33.86 FPS
10.17 FPS
10.44 FPS
10.88 FPS
10.84 FPS
10.86 FPS
10.86 FPS

Face Tracker


31.14 FPS
31.56 FPS
31.23 FPS
8.46 FPS
8.44 FPS
8.48 FPS
8.41 FPS
8.46 FPS
8.40 FPS

Face Recognizer2
30.40 FPS
28.93 FPS
29.87 FPS

9.77 FPS
9.77 FPS
9.97 FPS
9.86 FPS
9.78 FPS
9.79 FPS



APP
CPU
NCS2
WebCam  | Picam
NCS2 Optimizer

Object detector
Unsupported1
2.44       7.51
3.01       7.32
3.04       7.42


Face Tracker
Unsupported1



Face Recognizer2
Unsupported1
1.95       3.88
1.05       6.27
 


1: OpenVino does not support inferencing on arm64 CPU architecture at this time so the NCS2 or movidius is required on the RPi
2:
3:




FUTURE IDEAS/PROJECT EXTENSIONS
	Visualization Platform
      Object re-identification on single camera
	Object recognition on untrained cameras





