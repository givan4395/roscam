# CMake generated Testfile for 
# Source directory: /home/jmwolff3/catkin_ws/src
# Build directory: /home/jmwolff3/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(app)
subdirs(tutorial)
